<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\VirtualProxy\Model;

/**
 * Class RegularPoligon.
 *
 * This class acts as a base class for regular poligons virtual types.
 * For demo purposes, to justify a proxy usage, a slow loading is simulated by using sleep function.
 */
class RegularPolygon
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $sides;

    /**
     * RegularPolygon constructor.
     *
     * @param string $name
     * @param int $sides
     */
    public function __construct(
        string $name,
        int $sides
    ) {
        $this->name = $name;
        $this->sides = $sides;

        // Slow loading
        sleep(2);
    }

    /**
     * @param float $sideLength
     * @return float
     */
    public function getPerimeter(float $sideLength): float
    {
        return (float) $this->sides * $sideLength;
    }
}
