<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\VirtualProxy\Console\Command;

use Interactiv4\VirtualProxy\Model\RegularPolygon;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestCommand.
 *
 * Test command for virtual proxies.
 */
class TestCommand extends Command
{
    const COMMAND_NAME_PREFIX = 'interactiv4:virtual-proxy:';

    const OPTION_SIDE_LENGTH = 'side-length';

    const OPTION_SIDE_LENGTH_DEFAULT = 1;

    /**
     * @var RegularPolygon
     */
    private $regularPolygon;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        RegularPolygon $regularPolygon,
        string $name
    ) {
        $this->regularPolygon = $regularPolygon;
        parent::__construct(self::COMMAND_NAME_PREFIX . $name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->addOption(
            self::OPTION_SIDE_LENGTH,
            null,
            InputOption::VALUE_REQUIRED,
            'Side length',
            self::OPTION_SIDE_LENGTH_DEFAULT
        );

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $output->writeln($this->regularPolygon->getPerimeter((float) $input->getOption(self::OPTION_SIDE_LENGTH)));
    }
}
