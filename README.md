# Interactiv4 Module VirtualProxy

Description
-----------

This module is a sample module for testing virtual proxies.


Installation
-----------

Run following commands:

```
composer config repositories.virtual-proxy vcs https://bitbucket.org/interactiv4/module-virtual-proxy
composer require --dev 'interactiv4/module-virtual-proxy:@dev'
bin/magento setup:upgrade
```