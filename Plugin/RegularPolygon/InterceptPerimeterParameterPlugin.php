<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\VirtualProxy\Plugin\RegularPolygon;
use Interactiv4\VirtualProxy\Model\RegularPolygon;

/**
 * Class InterceptPerimeterParameterPlugin.
 *
 * Intercept perimeter parameter.
 */
class InterceptPerimeterParameterPlugin
{
    /**
     * @param RegularPolygon $regularPolygon
     * @param float $sideLength
     *
     * @return array
     */
    public function beforeGetPerimeter(
        RegularPolygon $regularPolygon,
        float $sideLength
    ) {
        // Modify side length
        $sideLength += 100;

        return [$sideLength];
    }
}
